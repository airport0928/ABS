# Windows 下安装

---

ABS 只支持 `Java8`，在编译前请设置环境变量 `JAVA_HOME` 指向JDK1.8

1、假设操作的更目录在  `E:\` 


2、clone代码

    git clone https://gitee.com/crm8000/ABS.git


3、编译Axelor SDK

    cd E:\ABS\sdk

    gradlew installDist

4、设置环境变量 `AXELOR_HOME`

    环境变量的值为：E:\ABS\sdk\build\install\axelor-development-kit

5、把`E:\ABS\sdk\build\install\axelor-development-kit\bin`加入到PATH环境变量中

    这样在命令行中就可以运行  axelor  命令

6、安装PostgreSQL 10

    新建数据库用户 abs_user 密码 abs_user

    新建数据库 abs ,该数据库的owner是abs_user

7、运行ABS

    cd E:\ABS\abs
    
    axelor run

8、打开浏览器访问：http://localhost:8080/abs

    登录系统的用户名 admin 密码 admin